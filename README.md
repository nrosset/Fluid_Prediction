This repository contains the code of the method described in our paper https://hal.science/hal-03975369v1

You will find:

* A folder Synchronyzation, where you will be able to run our algorithm to extract a representative frame from a pre-run simulation 
* A folder Shape optimization in which, based on our neural surrogate model, you will be ale to optimize the aerodynamics of a 2D car profile
