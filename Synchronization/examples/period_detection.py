import numpy as np
import matplotlib.pyplot as plt
import scipy.fftpack
from scipy import fftpack
import random
from statsmodels.tsa.stattools import acf


def periodic_corr(x, y):
    """Periodic correlation, implemented using the FFT.

    x and y must be real sequences with the same length.
    """
    return np.fft.ifft(np.fft.fft(x) * np.fft.fft(y).conj()).real

def periodic_corr_np(x, y):
    """Periodic correlation, implemented using np.correlate.

    x and y must be real sequences with the same length.
    """
    return np.correlate(x, np.hstack((y[1:], y)), mode='valid')

def get_freq_hints(time_series):
    x = time_series
    x_copy = x.copy()
    N = len(x)
    nb_permutations = 100
    sum = 0
    freqs_max = []
    for i in range(nb_permutations):
        x_copy = x.copy()
        random.shuffle(x_copy)
        FFT = np.fft.fft(x_copy)
        FFT_mag = np.abs(FFT)/N
        mag_max = np.max(FFT_mag[2:int(N/2+1)])
        sum += mag_max
        freqs_max.append(mag_max)
    sum /= N
    criterion = (99/100)*np.max(freqs_max)
    FFT = np.fft.fft(x)
    FFT_mag = np.abs(FFT)/N
    bool_list = FFT_mag > criterion
    bool_list = bool_list[:int(N/2+1)]
    mag_max = np.max(FFT_mag[2:int(N/2+1)])
    retained_freqs = np.where(bool_list==True)[0]
    retained_freqs = retained_freqs[retained_freqs > 1]
    retained_freqs = retained_freqs[retained_freqs < N]
    magnitude_values = [FFT_mag[f] for f in retained_freqs]
    return retained_freqs , magnitude_values


## NAIVE IMPLEMENTATION OF HILL CLIMBING
def hill_climbing(signal,first_guess):
    finished = False
    t = first_guess
    n = len(signal)
    if t==0:
        if signal[1] < signal[0]:
            return 0
        else:
            while finished==False:
                if signal[t+1] > signal[t]:
                    t = t+1
                else:
                    return t
    elif t==n-1:
        if signal[n-1] < signal[n]:
            return n
        else:
            while finished==False:
                if signal[t-1] > signal[t]:
                    t = t-1
                else:
                    return t
    else:
        if signal[t] > signal[t-1] and signal[t] > signal[t+1]:
            return t
        elif signal[t] < signal[t-1] and signal[t] < signal[t+1]:
            return t
        ## VALLEY
        else:
            if signal[t+1] > signal[t]:
                while finished==False:
                    if t==0 or t==n-1:
                        return t
                    elif signal[t+1] > signal[t]:
                        t = t+1
                    else:
                        return t
            if signal[t-1] > signal[t]:
                while finished==False:
                    if t==0 or t==n-1:
                        return t
                    elif signal[t-1] > signal[t]:
                        t = t-1
                    else:
                        return t

def get_refined_periods(signal):
    y = signal
    N = len(y)
    retained_freqs , magnitude_values = get_freq_hints(y)
    retained_periods = [int(N/f) for f in retained_freqs]
    ACP = acf(y,nlags=N)
    ACP_hints = np.array([[f,ACP[int(N/f)]] for f in retained_freqs])
    if len(ACP_hints) == 0:
        return []
    else:
        best_acp_freq = int(ACP_hints[np.argmax(ACP_hints[:,1])][0])
        FFT = np.fft.fft(y)
        FFT_mag = np.abs(FFT)/N
        FFT_mag_idxs = FFT_mag == FFT_mag[best_acp_freq]
        FFT_mag_idxs[0] = True
        FFT_clean = FFT_mag_idxs * FFT
        signal_filtered = np.fft.ifft(FFT_clean)
        T = np.linspace(0,N-1 , N)
        F = np.linspace(0,1/1900 , N)
        FFT = np.fft.fft(y)
        FFT_mag = np.abs(FFT)/N
        refined_periods = []
        for P in retained_periods:
            print(hill_climbing(ACP,P))
            refined_periods.append(hill_climbing(ACP,P))
        refined_periods = list(set(refined_periods))
        if 0 in refined_periods:
            refined_periods.remove(0)
        ACPs = [ACP[T] for T in refined_periods]
        if len(refined_periods) == 0:
            return []
        else:
            return [refined_periods , ACPs]


def compute_sampled_pressure_sum(pressure):
    pressure = pressure[0].detach().cpu().numpy()

    xs = [5+5*k for k in range(44)] ##SAMPLED POINT OVER WHICH WE PERFORM THE SUM
    ys = [300+5*k for k in range(0,50)]
    list_points = []
    for x in xs:
        for y in ys:
            list_points.append([x,y])
    sum = 0
    for point in list_points:
        sum += pressure[point[0],point[1]]
    return sum

def update_period_list(to_be_filled,signal):
    Ts = np.array(get_refined_periods(signal))
    print("Ts")
    print(Ts)
    print("current list")
    print(to_be_filled)
    if len(Ts) == 0:
        return np.array([])
    else:
        [periods , ACPs] = Ts
        max_idx = np.argmax(ACPs)
        current_T , current_ACP = periods[max_idx] , ACPs[max_idx]
        print("T")
        print([current_T , current_ACP])
        list_length = len(to_be_filled)
        if list_length == 0:
            to_be_filled = np.array([[current_T , current_ACP]])
        else:
            if (np.sum(to_be_filled[:,0])/list_length-8 <= current_T <= np.sum(to_be_filled[:,0])/list_length+8) and (np.sum(to_be_filled[:,1])/list_length-0.08 <= current_ACP <= np.sum(to_be_filled[:,1])/list_length+0.08):
                to_be_filled = np.append(to_be_filled,[[current_T , current_ACP]],axis=0)
            else:
                to_be_filled = np.array([[current_T , current_ACP]])
        print("final")
        print(to_be_filled)
        return to_be_filled
