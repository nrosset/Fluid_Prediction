import numpy as np
import torch
import imageio

import sys
sys.path.append('../')

import utils
import argparse
import os
import matplotlib.pyplot as plt
from numpy import linalg as LA
import period_detection

## This code loaded the frames or a pre-run simulation and extracts the synchronized frame as described in our method..
## The simulation was run in 2D with a domain of size (256,600), an obstacle described by the SDF here loaded. Further details are mentionned in the paper.
## The plots of pressure forces stored. Those can help to assess if the simulation was well generated.

def simulator(
    total_steps,
    sdf_file):

    sdf_np = np.load(sdfs_folder + str(sdf_file) + '.npy')
    sdf_np = np.flip(sdf_np,axis=0) ## The sdf with which the simulation was generated, here optional since the frames are directly loaded
    list_sampled_pressure_sum = []
    stored_pressures = {}
    stored_velocities = {}
    to_be_filled = np.array([])
    dir_name = "./extracted_frames/extracted_frame_"+str(sdf_file)
    utils.mkdir(dir_name)

    for step in range(total_steps):

        ## The loading of frames here replaces your solver for generating velocities and pressure at each time step
        print("demo: simulate at frame {}.".format(step))
        vel , pressure = np.load("./frames/"+"vel_{:03}.npy".format(step + 1)) , np.load("./frames/"+"presure_{:03}.npy".format(step + 1))
        vel , pressure = torch.Tensor(vel).cuda() , torch.Tensor(pressure).cuda()

        ## Fourier analysis for analyzing the signal
        sampled_pressure_sum = period_detection.compute_sampled_pressure_sum(pressure)
        list_sampled_pressure_sum.append(sampled_pressure_sum)
        stored_pressures[step] = pressure.detach().cpu().numpy()
        stored_velocities[step] = vel.detach().cpu().numpy()

        ## We stop the simulation after 6000 steps if the criterion was not satisfied before
        if step == total_steps-1:
            plt.plot(list_sampled_pressure_sum,label="Sampled sum of pressures",color="blue")
            pressure_plot_file = './Pressure_plots_new_criterion/Resulting_pressure_forces_'+str(sdf_file)+'.png'
            plt.savefig(pressure_plot_file)
            plt.close()

        ## Every 50 frame and starting from 600 frames, we assess the validity of the criterion, and wait for it to be satisfied 5 times in a row
        elif step>=600 and step%50==0:
            signal = list_sampled_pressure_sum[-600:]
            to_be_filled = period_detection.update_period_list(to_be_filled,signal)

            if len(to_be_filled)==4:
                T_computed = to_be_filled[0][0]
                max_idx = np.argmax(list_sampled_pressure_sum[-int(T_computed):])+step-int(T_computed)
                previous_max_idx = period_detection.hill_climbing(list_sampled_pressure_sum,max_idx-int(to_be_filled[0][0]))
                norm2 = LA.norm(stored_velocities[previous_max_idx]-stored_velocities[max_idx])
                print("Norm 2 difference between the two consecutive velocity fields :{:.2f}".format(norm2))

                if norm2 > 20:
                    to_be_filled = to_be_filled[1:]
                    continue
                else:
                    print("The criterion was fully satisfied, the frame is stored")
                    plt.plot(list_sampled_pressure_sum,label="Sampled sum of pressures",color="blue")
                    plt.legend(loc='best')
                    plt.plot(max_idx,list_sampled_pressure_sum[max_idx],marker="+",color="red")
                    plt.plot(previous_max_idx,list_sampled_pressure_sum[previous_max_idx],marker="+",color="red")
                    pressure_plot_file = './Pressure_plots_new_criterion/Resulting_pressure_forces_'+str(sdf_file)+'.png'
                    plt.savefig(dir_name+"./pressure_signal_evolution")
                    plt.close()
                    np.save(dir_name+"/pressure",stored_pressures[max_idx])
                    np.save(dir_name+"/velocity",stored_velocities[max_idx])
                    np.save(dir_name+"/velocity_previous_peak",stored_velocities[previous_max_idx])
                    break


sdfs_folder = '../sdfs_shape/'
list_sdfs = os.listdir(sdfs_folder)

for sdf_file in list_sdfs:
    file = sdf_file[:-4]
    simulator(6000,
        file
    )
