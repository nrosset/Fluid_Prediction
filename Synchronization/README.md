Based on a pre-run simulation over the sdf contained in ./sdfs_shape/, we propose an Fourier based algorithm to study the evolution of pressure in the domain to extract a representative frame.

You can try it by running 
python generate_synchronized_frame.py

This will save the extracted frame together with a plot of the evolution of our pressure coefficient which is used to study the periodicity of the simulation.

All details about the simulation and the algorithm are described in https://hal.science/hal-03975369v1 as well.
