import torch
import torch.nn.functional as F
import math
import matplotlib.pyplot as plt
import physics_learning.data_processing_
import matplotlib.lines as mlines



def grad_sdf(sdf, coords, normalize = True, grad_reuse = True):

    normals = torch.autograd.grad(outputs=sdf , inputs=coords , grad_outputs=torch.ones(sdf.shape).cuda(), create_graph = grad_reuse )[0]

    if normalize:
        normals = normals/torch.norm(normals, dim = -1)[:, None]

    return normals

def dirac_gauss(sigma):
    return lambda x: 1/(sigma*math.sqrt(2*math.pi)) * torch.exp(-x**2/2/sigma**2)



def vorticity(vel, coords, dx_o_dy):

    dvxdy = torch.autograd.grad(outputs=vel[..., 0], inputs=coords, grad_outputs=torch.ones(vel[..., 0].shape).cuda(), create_graph =True )[0][..., [0]]
    dvydx = torch.autograd.grad(outputs=vel[..., 1], inputs=coords, grad_outputs=torch.ones(vel[..., 1].shape).cuda(), create_graph =True )[0][..., [1]]

    ## Warning: dx and dy are not the same with numerical gradients!! need to scale dvxdy
    return dvydx - dvxdy/dx_o_dy

def numerical_vorticity(vel, dx):
    dvxdy = vel[..., 2:, 1:-1, 0] - vel[..., :-2, 1:-1, 0]
    dvydx = vel[..., 1:-1, 2:, 1] - vel[..., 1:-1, :-2, 1]

    # requires input to be (H, W, 2)
    return F.pad((dvydx - dvxdy)[None, None] /2/dx, (1, 1, 1, 1))

def cumulative_of_gaussian(x , sigma):
    # print("a")
    # print(x.shape)
    # print(torch.erf(x).shape)
    return 1 - 1/2*(1 + torch.erf((x)/sigma/math.sqrt(2)))

def numerical_grad(sdf, dx, normalize = True):
    # requires input to be (H, W)
    dsdy = sdf[..., 2:, 1:-1] - sdf[..., :-2, 1:-1]
    dsdx = sdf[..., 1:-1, 2:] - sdf[..., 1:-1, :-2]

    tmp = torch.moveaxis(torch.stack((dsdx, dsdy), axis = -1), -1, 0)
    out =  torch.moveaxis(F.pad(tmp[None] /dx, (1, 1, 1, 1)), 1, -1) /2/dx

    if normalize:
        out = out / (1e-6+torch.norm(out, dim = -1)[..., None])

    return out

# def run_optim(opti_coef , custom_idx , lr , beta , optim_lr_decay , optim_max_iter , optim_change_tol , thetas , shape , net_sdf , net_pressure , net_vel , vort_mask=None):
#
#         coords = physics_learning.data_processing_.get_mgrid(shape).cuda()
#         coords = coords.cuda()
#         theta = torch.Tensor(thetas[custom_idx]).unsqueeze(0).cuda()
#         losses = []
#         losses_drag = []
#         thetas_path = []
#
#         def closure(x , normalization = 1 , need_grad = False):
#
#             loss = 0
#             if opti_coef=="drag":
#                 loss = loss + drag_loss(x, coords, need_grad , beta , losses_drag , net_sdf , net_pressure)
#             if opti_coef=="vorticity":
#                 loss = loss + vort_loss(x, coords, need_grad , beta , losses_drag , net_sdf , net_pressure)
#
#             return loss/normalization
#
#         for i in range(optim_max_iter):
#
#             thetas_path.append(theta)
#             theta_optim = theta.clone().requires_grad_(True)
#             loss = closure(theta_optim , normalization = 1 , need_grad = True)
#
#             if i==0:
#                 loss_origin = loss.detach().clone()
#
#             loss = loss / loss_origin
#
#             losses.append(loss.item())
#             losses_drag.append(global_drag_loss)
#
#             if len(losses) >2 and losses[-2] - losses[-1] < optim_change_tol:
#                 break
#
#             grads = torch.autograd.grad(loss, theta_optim, torch.ones_like(loss))[0].detach()
#             loss = loss.detach()
#
#             use_line_search = True
#             if use_line_search:
#                 theta, _ = optim_with_linesearch(theta, loss, grads, lambda x: closure(x , loss_origin), learning_rate = lr)
#             else:
#                 theta -= lr*grads
#
#             lr *= optim_lr_decay
#
#         fig, ax = plt.subplots()
#         color = 'tab:blue'
#         ax.set_ylabel('drag loss', color=color)  # we already handled the x-label with ax1
#         ax.plot(losses_drag, color=color)
#         ax.tick_params(axis='y', labelcolor=color)
#         fig.tight_layout()  # otherwise the right y-label is slightly clipped
#         plt.close()
#
#         return thetas_path

def run_optim(opti_coef , custom_idx , lr , beta , optim_lr_decay , optim_max_iter , optim_change_tol , thetas , shape , net_sdf , net_pressure , net_vel , vort_mask=None):

        coords = physics_learning.data_processing_.get_mgrid(shape).cuda()
        coords = coords.cuda()
        theta = torch.Tensor(thetas[custom_idx]).unsqueeze(0).cuda()
        losses = []
        losses_drag = []
        thetas_path = []

        def closure(x , normalization = 1 , need_grad = False):

            loss = 0
            if opti_coef=="drag":
                loss = loss + drag_loss(x, coords, need_grad , beta , losses_drag , net_sdf , net_pressure)
            if opti_coef=="vorticity":
                loss = loss + vort_loss(x, coords, need_grad , beta , losses_drag , net_vel , vort_mask)

            return loss/normalization

        for i in range(optim_max_iter):

            thetas_path.append(theta)
            theta_optim = theta.clone().requires_grad_(True)
            loss = closure(theta_optim , normalization = 1 , need_grad = True)

            if i==0:
                loss_origin = loss.detach().clone()

            loss = loss / loss_origin

            losses.append(loss.item())

            if len(losses) >2 and losses[-2] - losses[-1] < optim_change_tol:
                break

            grads = torch.autograd.grad(loss, theta_optim, torch.ones_like(loss))[0].detach()
            loss = loss.detach()

            use_line_search = True
            if use_line_search:
                theta, _ = optim_with_linesearch(theta, loss, grads, lambda x: closure(x , loss_origin), learning_rate = lr)
            else:
                theta -= lr*grads

            lr *= optim_lr_decay

        fig, ax = plt.subplots()
        color = 'tab:blue'
        ax.set_ylabel('drag loss', color=color)  # we already handled the x-label with ax1
        ax.plot(losses_drag, color=color)
        ax.tick_params(axis='y', labelcolor=color)
        fig.tight_layout()  # otherwise the right y-label is slightly clipped
        plt.close()

        return thetas_path , losses

def drag_loss(x, coords, need_grad , beta , losses_drag , net_sdf , net_pressure):

    coords = coords.clone().requires_grad_(True)
    grid = physics_learning.data_processing_.get_multiple_mgrid(x, coords)[0]

    dx = 1/600
    sidelen = (256,600)

    sdf = net_sdf(grid, unnormalize = True)
    pressure = net_pressure(grid, unnormalize = True)

    dirac = dirac_gauss(1.2*dx)
    normals = -grad_sdf(sdf, coords, normalize=True, grad_reuse = need_grad)

    # normals , dirac_over_sdf , pressure = normals.view(sidelen[0], sidelen[1], 2) , dirac(sdf).view(sidelen[0],sidelen[1]) , pressure.view(sidelen[0],sidelen[1])
    dirac_over_sdf = dirac(sdf)

    opti_loss = torch.sum(normals * dirac_over_sdf*pressure)**2


    if not need_grad:
        opti_loss = opti_loss.detach()

    global global_drag_loss
    global_drag_loss = opti_loss.item()



    return opti_loss

# def vort_loss(x, coords, need_grad , beta , losses_drag , net_vel , vort_mask):
#
#     coords = coords.clone().requires_grad_(True)
#     grid = phy_learning.data_processing_.get_multiple_mgrid(x, coords)[0]
#
#     if hasattr(shape_optim_window, 'vort_mask'):
#         # mask = shape_optim_window.vort_mask.reshape(-1)
#         mask = vort_mask.reshape(-1)
#         restricted_coords = coords[mask > .5]
#         restricted_grid = phy_learning.data_processing_.get_multiple_mgrid(x, restricted_coords)[0]
#         restricted_vel = app.net_vel(restricted_grid, unnormalize = True)
#         restrcited_vort = optim_utils.vorticity(restricted_vel, restricted_coords, 256/600)
#
#         loss = torch.sum(restrcited_vort.reshape(-1)**2) / torch.sum(mask)
#
#     else:
#
#         vel = net_vel(grid, unnormalize = True)
#         vort = optim_utils.vorticity(vel, coords, 256/600)
#
#         loss = torch.mean(vort.reshape(-1)**2)
#
#     if not need_grad:
#         loss = loss.detach()
#
#     return loss

def vort_loss(x, coords, need_grad , beta , losses_drag , net_vel , vort_mask):

    coords = coords.clone().requires_grad_(True)
    grid = physics_learning.data_processing_.get_multiple_mgrid(x, coords)[0]

    if vort_mask!=None:
        # mask = shape_optim_window.vort_mask.reshape(-1)
        mask = vort_mask.reshape(-1)
        restricted_coords = coords[mask > .5]
        restricted_grid = physics_learning.data_processing_.get_multiple_mgrid(x, restricted_coords)[0]
        restricted_vel = net_vel(restricted_grid, unnormalize = True)
        restrcited_vort = vorticity(restricted_vel, restricted_coords, 256/600)

        loss = torch.sum(restrcited_vort.reshape(-1)**2) / torch.sum(mask)

    else:

        vel = net_vel(grid, unnormalize = True)
        vort = vorticity(vel, coords, 256/600)

        loss = torch.mean(vort.reshape(-1)**2)

    if not need_grad:
        loss = loss.detach()

    return loss

def visu_contour(list_thetas , coords , net_sdf):
    dx = 1/600
    sidelen = (256,600)
    theta_init = list_thetas[0].detach()
    theta_final = list_thetas[-1].detach()
    grid_init = physics_learning.data_processing_.get_multiple_mgrid(theta_init, coords)[0]
    sdf_init = net_sdf(grid_init, unnormalize = True).detach().cpu()
    grid_final = physics_learning.data_processing_.get_multiple_mgrid(theta_final, coords)[0]
    sdf_final = net_sdf(grid_final, unnormalize = True).detach().cpu()

    hmax , wmin , wmax = 100 , 30 , 300
    restricted_sdf_init = sdf_init.reshape(256,600)[:hmax , wmin:wmax]
    restricted_sdf_final = sdf_final.reshape(256,600)[:hmax , wmin:wmax]
    restricted_shape = (hmax , wmax-wmin)
    fig, ax = plt.subplots(figsize=(8, 4))
    cs0 = ax.contour(restricted_sdf_init.reshape(list(restricted_shape)).detach().cpu(), levels = [0], linewidths = 1, colors = 'red')
    cs1 = ax.contour(restricted_sdf_final.reshape(list(restricted_shape)).detach().cpu(), levels = [0], linewidths = 1, colors = 'green')
    red_line = mlines.Line2D([], [], color='red', marker='_', alpha=0.5,
                          markersize=15, label='initial shape')
    green_line = mlines.Line2D([], [], color='green', marker='_', alpha=0.5,
                          markersize=15, label='optimized shape')
    ax.legend(handles=[red_line , green_line])
    plt.axis("off")

## linesearch

def optim_with_linesearch(u0, Fu0, grad, closure, learning_rate = 1, alpha = 1e-4, tol = 1e-8):

    delta_u_max_factor = 100
    norm_u = torch.norm(u0)
    norm_delta_u_max = delta_u_max_factor * max(norm_u, 1)
    eps = torch.finfo(u0.dtype).eps

    delta_u = -grad

    Tu0 = .5 * Fu0*Fu0
    err = math.sqrt(2*Tu0)

    if err < tol:
        return u0, err

    norm_delta_u = torch.norm(delta_u)
    if norm_delta_u > norm_delta_u_max:
        delta_u *= norm_delta_u_max / norm_delta_u

    slope = -torch.dot(delta_u.reshape(-1), delta_u.reshape(-1)) * Fu0
    assert slope < 0, 'slope'


    # lambda_min
    if norm_u < norm_delta_u:
        norm_u = norm_delta_u
    l_min = eps * norm_u / norm_delta_u

    assert l_min<1

    l = learning_rate
    l_prev = 0
    Tu_prev = 0

    iters = 0

    while(True):
        if l<l_min:
            # local min
            break

        u = u0 + l * delta_u
        Fu = closure(u)
        Tu = .5*Fu*Fu


        if Tu <= Tu0 + alpha * l * slope:
            break

        if iters == 0:
            # assume g(l) = (g(lr) - g(0) - g'(0)) (l/lr)**2 + g'(0) (l/lr) + g(0)
            # g'(l) = 2/lr**2  (g(lr) - g(0) - g'(0)) l + g'(0)/lr
            # g'(l) = 0 =>
            l_next = -.5 * slope * learning_rate / (Tu - Tu0 - slope)
        else:
            # https://cel.archives-ouvertes.fr/cel-00573970/document , p127

            z = Tu - Tu0 - l * slope
            z_prev = Tu_prev - Tu0 - l_prev * slope
            a = (z / l**2 - z_prev / l_prev **2) /  (l-l_prev)
            b = ( - z * l_prev / l**2 + z_prev * l / l_prev **2) /  (l-l_prev)

            if a == 0:
                l_next = -.5*slope / b
            else:
                delta = b**2 - 3*a*slope
                if delta <0:
                    print(delta, b, a, slope, l)
                assert delta >= 0
                l_next = (-b + math.sqrt(delta))/3/a

            l_next = min(l / 2, l_next)

        l_prev = l
        Tu_prev = Tu
        l = max(l/10, l_next)

        iters += 1

    return u, err
