from re import A
import torch
from torch import nn
import torch.nn.functional as F
from torch.utils.data import DataLoader, Dataset
import numpy as np

import numpy as np

from torchvision.transforms import Resize, Compose, ToTensor, Normalize
import numpy as np
from tqdm import tqdm
import os

def get_mgrid(sidelen):
    '''
    Generates a flattened grid of (x,y,...) coordinates in a range of -1 to 1.
    sidelen: int (dimensions (x, y) of the grid)
    outputshape: (product(sidelent), len(sidelen))
    '''
    mgrid = torch.stack(
              torch.meshgrid( [torch.linspace(-1, 1, steps=sidelen[k]) for k in [0,1]], indexing = 'ij'), 
              dim=-1)
                  
    return mgrid.reshape(-1, len(sidelen))


def get_multiple_mgrid(theta_values, coords):
    """
    Returns the set of concatenated coordinates + code values
    theta_values: (num_theta, len_theta) tensor of thetas
    coords (num_coords, len_coords) tensor of coords
    output: (num_theta, num_coords, len_theta + len_coords)
    """

    return torch.cat( [
      coords.unsqueeze(0).repeat(theta_values.shape[0], 1, 1),
      theta_values.unsqueeze(1).repeat(1, coords.shape[0], 1)
    ], dim = -1)
    

def get_theta_values_from_path(data_name):
    return torch.tensor([
      float(el)
      for el in data_name.split("frame_[")[1].split("]")[0].split(", ")
    ])

def get_theta_values(list_training_samples):
    return torch.stack([
      get_theta_values_from_path(data_name)
      for data_name in list_training_samples
    ], dim = 0)


def get_image_tensor(path_to_image, sidelength):
    # loaded np array has shape (H, W, 2) but ToTensor shuffle it to (2, H, W)
    out =  Compose([
        ToTensor(),
        Resize(sidelength)
    ]) (np.load(path_to_image)).permute(1, 2, 0)

    # we also reshape it to (-1, n) for consistency with get_mgrid
    return out.reshape(-1, out.shape[-1])  


def get_pixels_from_paths(sidelength, path_to_folder, training_paths):

    """
    load the data values and output it as a (num_images, num_pixels, features_per_pixel) tensor
    """

    return torch.stack(
      [get_image_tensor(os.path.join(path_to_folder, local_path), sidelength) for local_path in tqdm(training_paths)],
      dim = 0
    )


def get_max_values_per_channel(path_to_correspondances,output_dim,training_paths,sidelength):
  global_max_values = [0]*output_dim
  for path in training_paths:
      np_file = np.load(path_to_correspondances+path)
      transform = Compose([
        ToTensor(),
        Resize(sidelength)
      #        Normalize(torch.Tensor([0.6685,0.0043,-0.0237]), torch.Tensor([0.0821,0.1364,0.1275]))
      #        Normalize(torch.Tensor([0.6685,0.0043,-0.0237]), torch.Tensor([0.0821,0.1364,0.1275]))
      ])
      tensor = transform(np_file)
      max_values = [max(tensor[k].max(),(-tensor[k]).max()) for k in range(output_dim)]
      for k in range(output_dim):
        if max_values[k]>global_max_values[k]:
          global_max_values[k]=max_values[k]
  return global_max_values



class ThetaWrapperDataset:

    def __init__(self, sidelength, path_to_folder, training_paths, normalize = True):

      """
      sidelength: shape of the images (dataset is resized)
      coords and pixels are stored as linear pixel arrays (N, W*H, C)
      """
      print("[Loading data]")

      self.sidelength = sidelength
      self.thetas = get_theta_values(training_paths)
      print(f" - num images: {self.thetas.shape[0]}")
      print(f" - theta size: {self.thetas.shape[1]}")

      print(f" - creating coords ...", end = '', flush = True)
      self.ref_coords = get_mgrid(sidelength)
      self.coords = get_multiple_mgrid(self.thetas, self.ref_coords)
      print(f" done {tuple((i for i in self.coords.shape))}")

      print(" - loading pixels")
      self.pixels = get_pixels_from_paths(sidelength, path_to_folder, training_paths)
      self.normalization_values = torch.max(torch.abs(self.pixels.reshape(-1, self.pixels.shape[-1])), dim = 0).values
      self.normalized = normalize
      if self.normalized:
        self.pixels/= self.normalization_values
      print(f" pixels loaded {tuple((i for i in self.pixels.shape))}")


    def save(self, filename):
      np.savez_compressed(filename, 
        thetas = self.thetas, 
        pixels = self._as_image(self.pixels * self.normalization_values if self.normalized else self.pixels))


    def _as_image(self, data):
      """
      returns data as batches of images (N, H, W, C)
      """
      return data.reshape(data.shape[0], *self.sidelength, data.shape[2])

    def coords_as_images(self):
      return self._as_image(self.coords)

    def pixels_as_images(self):
      return self._as_image(self.pixels)

    def _mixed(self, data):
      return data.reshape(-1, data.shape[-1])

    def coords_mixed(self): 
      return self._mixed(self.coords)
    
    def pixels_mixed(self): 
      return self._mixed(self.pixels)

    def split(self, fract):
      class _SplitDataset(ThetaWrapperDataset):
        def __init__(self, parent, idx):
          self.thetas = parent.thetas[idx]
          self.sidelength = parent.sidelength
          self.ref_coords = parent.ref_coords
          self.coords = parent.coords[idx]
          self.pixels = parent.pixels[idx]
          self.normalization_values = parent.normalization_values
          self.normalized = parent.normalized

      pivot = int(self.thetas.shape[0] * fract)
      return (_SplitDataset(self, torch.arange(pivot)), 
              _SplitDataset(self, torch.arange(pivot, self.thetas.shape[0])))


    def append_grad(self, channels):

      # append the numerical gradients to the pixels of the dataset, and returns the tuple of the indices of the corresponding gradients in the dataset

      self.grad_from = self.pixels.shape[-1]

      dx = float(self.coords_as_images()[0, 0, 1, 1]- self.coords_as_images()[0, 0, 0, 1])
      dy = float(self.coords_as_images()[0, 1, 0, 0]- self.coords_as_images()[0, 0, 0, 0])
      print(f"Dataset: add grads for dimensions {tuple(channels)}, with spacing {dx},{dy}")
      print(f" - check (should be around 2: {dx*self.sidelength[1]}, {dy*self.sidelength[0]}")
      self.pixels = torch.cat([
        self.pixels, 
        *[t.reshape(t.shape[0], -1, t.shape[-1]) for t in torch.gradient(self.pixels_as_images()[..., channels], spacing = [dx, dy], dim = [2, 1])]
      ], dim = -1)

      print(f" - New shape {self.pixels.shape}")


class CompressedThetaWrapperDataset(ThetaWrapperDataset):

  def __init__(self, filename, normalize = True, compute_coords = True):

    print("[Loading compressed data]")

    data = np.load(filename)
    self.thetas = torch.Tensor(data['thetas'])
    print(f" - num images: {self.thetas.shape[0]}")
    print(f" - theta size: {self.thetas.shape[1]}")

    print(" - loading pixels")
    self.pixels = torch.Tensor(data['pixels'])
    self.sidelength = self.pixels.shape[1:3]
    self.pixels = self.pixels.reshape(self.pixels.shape[0], -1, self.pixels.shape[-1])
    self.normalization_values = torch.max(torch.abs(self.pixels.reshape(-1, self.pixels.shape[-1])), dim = 0).values
    self.normalized = normalize
    if self.normalized:
      self.pixels/= self.normalization_values
    print(f" pixels loaded {tuple((i for i in self.pixels.shape))}")
    
    if compute_coords:
      print(f" - creating coords ...", end = '', flush = True)
      self.ref_coords = get_mgrid(self.sidelength)
      self.coords = get_multiple_mgrid(self.thetas, self.ref_coords)
      print(f" done {tuple((i for i in self.coords.shape))}")
    else:
      print(f" - creating coords skiped")



def DatasetIterator(dataset, batch_size, shuffle_batches=False, shuffle_samples=False):

    n_batches = num_batches(dataset, batch_size)
    batch_order  = torch.randperm(n_batches) if shuffle_batches else torch.arange(n_batches)

    # TODO: this does not preserve the order of the dataset
    if shuffle_samples:
      samples_suffle = torch.randperm(dataset.coords_mixed().shape[0])
      dataset.coords_mixed()[:] = dataset.coords_mixed()[samples_suffle]
      dataset.pixels_mixed()[:] = dataset.pixels_mixed()[samples_suffle]
    
    for idx in batch_order:
        yield dataset.coords_mixed().view(n_batches, batch_size, -1)[idx] ,\
              dataset.pixels_mixed().view(n_batches, batch_size, -1)[idx]

def num_batches(dataset, batch_size):
    n_data = dataset.coords_mixed().shape[0]
    assert n_data % batch_size == 0, "Batch size should divide data size"
    return n_data // batch_size