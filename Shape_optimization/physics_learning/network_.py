import torch
from torch import nn
import torch.nn.functional as F
import numpy as np

def pos_encoding(coords, num_frequencies):

    # coords (..., n_coords, 1)
    # freq_id (..., 1, n_freq)

    freq_id = torch.arange(num_frequencies).reshape(*([1]*len(coords.shape)), -1).to(coords.device).to(coords.dtype)
    coords = coords.unsqueeze(-1)

    return torch.cat([
        coords,
        torch.sin((2 ** freq_id) * np.pi * coords),
        torch.cos((2 ** freq_id) * np.pi * coords),
    ], dim = -1).reshape(*coords.shape[:-2], -1)

## Reminder for all experiments (TDOD: delete it?)
#            sin = torch.unsqueeze(torch.sin(c/(np.float(1000)**np.float((4*i/num_frequencies)))), -1)
#            cos = torch.unsqueeze(torch.cos(c/(np.float(1000)**np.float((4*i/num_frequencies)))), -1)



def init_weights_normal(m):
    nn.init.kaiming_normal_(m.weight, a=0.0, nonlinearity='relu', mode='fan_in')

def init_weights_xavier(m):
    nn.init.xavier_uniform_(m.weight, gain=1.0)


class Lip_layer(nn.Module):
    def __init__(self, in_features, out_features):
        super().__init__()
        self.layer = nn.Linear(in_features , out_features , bias=True)

        self.weight = self.layer.weight
        self.ci = nn.Parameter(torch.Tensor([1.0]))

    def forward(self, x):
        absrowsum = torch.sum(torch.abs(self.weight) , axis=1)
        scale = torch.clamp(F.softplus(self.ci)/absrowsum , max=1)

        return F.linear(x, self.weight*scale[:, None], self.layer.bias)


class MyNet(nn.Module):

    def __init__(self, dim_theta , output_dim, opt):
        super().__init__()

        self.num_frequencies = opt.frequencies
        out_dim_theta = dim_theta + 2 + 2*2*self.num_frequencies
        self.dim_theta = dim_theta
        self.config_weight_init = opt.weight_init
        self.config_layer_type  = opt.layer_type
        self.skip_connection = opt.skip_connection
        self.dropout_probability = opt.dropout_probability
        self.use_silu = opt.use_silu

        self.ci_list = []

        self.register_buffer("normalization", torch.ones(output_dim, dtype = float))

        self.layers = nn.ModuleList(
            [self.add_and_register_layer(out_dim_theta, 256)] + 
            [self.add_and_register_layer(256 + (out_dim_theta if i == opt.skip_connection else  0), 256) for i in range(opt.num_layers)] +
            [self.add_and_register_layer(256, output_dim , activation=False)]
        )

    def set_normalization(self, normalization):
        self.register_buffer("normalization", normalization)

    def add_and_register_layer(self, in_features, out_features, activation = True):

        if self.config_layer_type == 'default':
            l = nn.Linear(in_features, out_features)
        elif self.config_layer_type == 'lip':
            l = Lip_layer(in_features, out_features)
            self.ci_list.append(l.ci)
        elif self.config_layer_type == 'weight_norm':
            l = nn.utils.weight_norm(nn.Linear(in_features, out_features))
        else:
            raise ValueError(f"Wrong value for MyNet.config_layer_type: {self.config_layer_type}")

        if self.config_weight_init == "kaiming":
            nn.init.kaiming_normal_(l.weight, a=0.0, nonlinearity='relu', mode='fan_in')

        return nn.Sequential(l, nn.SiLU() if self.use_silu else nn.ReLU()) if activation else l

    def get_lip_constants_product(self):
        return torch.prod(F.softplus(torch.cat(self.ci_list))) if self.config_layer_type == 'lip' else 0

    def forward(self, inputs , unnormalize = False):

        x_coords = inputs[..., :-self.dim_theta]
        theta_coords = inputs[..., -self.dim_theta:]

        assert theta_coords.shape[-1] == self.dim_theta

        input = torch.cat([pos_encoding(x_coords, self.num_frequencies), theta_coords], dim = -1)
        out = input.clone()

        for k, layer in enumerate(self.layers):
            out = layer(
                out if self.skip_connection != max(0, k-1) else torch.cat([out, input], dim = -1)
            )
            if self.dropout_probability > 0:
                out = F.dropout(out, p=self.dropout_probability, training=self.training)

        if unnormalize:
            return out * self.normalization
        else:
            return out
