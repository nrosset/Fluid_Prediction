import os
import json
import torch
import network_

    
class _Arg_helper:
    def __init__(self, dict):
        self.dict = dict
    def __getattr__(self, name):
        return self.dict[name]

def load_network(path):
    """
    Load a network in folder 'path', that shoudl contain:
     - a file "opt_arguments.json" containing the necessary information to load the newtork
     - a .pth file (if many, the last one in alphabetical order will be chosen)
    """


    # load arguments
    with open(os.path.join(path, "opt_arguments.json"), 'r') as f:
        args = json.load(f)

    # create network
    model = network_.MyNet(args["dim_thetas"], args["dim_channels"], _Arg_helper(args['opt']))
    
    # find network data
    network_path = sorted([ f for f in os.listdir(path) if len(f) > 4 and f[-4:] == '.pth' ])[-1]

    # load network data
    # model.register_buffer("normalization", torch.ones(2, dtype = float)) ## TODO: <- delete this later, easy fix for normalization shape issue on trained models
    model.load_state_dict(torch.load(os.path.join(path, network_path)))
    # model.register_buffer("normalization", model.normalization[0:1]) ## TODO: <- delete this later, easy fix for normalization shape issue on trained models
    # torch.save(model.state_dict(),os.path.join(path, network_path)) ## TODO: <- delete this later, easy fix for normalization shape issue on trained models


    return model